#!/usr/bin/env python

# Тази програма преобразува YAML в XML.
# Трябва да бъде извикана с два входни параметъра:
# входния YAML файл и изходния XML файл

# трябва да са инсталирани pyyaml и dict2xml
import sys
import yaml
from dict2xml import dict2xml

# прочитаме имената на двата файла, подадени при извикването на тази програма
infile = sys.argv[1]
outfile = sys.argv[2]

# прочитаме данните от входния YAML файл
with open(infile, "r") as file:
  data_yaml = yaml.safe_load(file)
  
# преобразуваме ги в XML
data_xml = dict2xml(data_yaml)
  
# записваме ги в изходния XML файл
xml_preamb = '<?xml version="1.0" encoding="UTF-8"?>\n'
with open(outfile, "w") as file:
    file.write(xml_preamb)
    file.write(data_xml)
